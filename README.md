# Play with Python

To see the current version of deployed aplication open - [https://infotech-g.gitlab.io/play-with-python/](https://infotech-g.gitlab.io/play-with-python/)

To see documentation open - [https://infotech-g.gitlab.io/play-with-python/docs/](https://infotech-g.gitlab.io/play-with-python/docs/)

Available actions for development proccess:

## Available Scripts

In the project directory, you can run:

### `npm install`

Install all required dependencies. You need to run this command before running the project.

Remember that you also need to have installed Node JS (app is compatible with version 16.18).

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm run styleguide`

Runs the docs
Open [http://localhost:6060](http://localhost:6060) to view it in your browser.

The page will reload when you make changes.\
When new files are added it may need restart. \
You may also see any lint errors in the console.


## Authors

This project was originally made for Code Week contest of Zespół Szkół INFOTECH by:

- Adam Czarkowski
- Norbert Szorc
- Olgierd Kornacki
