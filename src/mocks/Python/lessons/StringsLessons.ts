import { EAdditionalNoteType } from 'models/additionalNoteType'
import { ILesson } from 'models/lesson'

export const StringLessons: ILesson[] = [
  {
    name: 'Ciągi znaków',
    note: {
      title: 'Ciągi znaków',
      content: `
        Stringi poznaliśmy w module 1, ale teraz czas na bliższe zapoznanie się. <br> <br>

        Mówiąc najprościej, jeśli chcesz użyć tekstu w Pythonie, musisz użyć <b>string</b>. <br> <br>
        Ale jest w tym o wiele więcej, o czym przekonamy się razem!

        Możemy utworzyć <b>string</b>, wpisując tekst między <b>dwoma pojedynczymi lub podwójnymi cudzysłowami</b>. W taki sposób: <br> <br>

        <div class="code">
        print("Python jest fajny!") <br> <br>
        print('Zawsze patrz na jasną stronę życia')
        </div>
        `,
      additionalNote: {
        content: `Symbol " lub ' używany w stringu nie wpływa w żaden sposób na jego zachowanie.`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Który z poniższych stringów został poprawnie skonstruowany?
      `,
      answers: [
        { text: `"Heja'`, isCorrect: false },
        { text: 'Heja', isCorrect: false },
        { text: `"Heja"`, isCorrect: true },
        { text: `'Heja"`, isCorrect: false }
      ]
    }
  },
  {
    name: 'Nowe linie',
    note: {
      title: 'Nowe linie',
      content: `
        Ok, więc możemy wygenerować tekst, ale byłoby to dość trudne do odczytania, gdyby wszystko było w jednej linii, prawda? <br> <br>

        Aby utworzyć nową linię, używamy <b>\\n</b>. <br> <br>

        <b>\\n</b> może być używany w ciągach do tworzenia wieloliniowych tekstów. <br> <br> 
        
        <b> Na przykład w taki sposób: </b>

        <div class="code">print('Raz \\n Dwa \\n Trzy') </div>
        `,
      additionalNote: {
        content: `Na podstawie tego, czego się właśnie dowiedzieliśmy, czy możesz zgadnąć, jak reprezentujemy tabulator? Zgadza się, to <b>\\t.</b>`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
      Która z poniższych opcji daje dokładnie dwie linie?
      `,
      answers: [
        { text: `"Hello world"`, isCorrect: false },
        { text: `"Hello \\n world"`, isCorrect: true },
        { text: `"raz \\n dwa \\n trzy"`, isCorrect: false },
        { text: `"raz \\t dwa"`, isCorrect: false }
      ]
    }
  },
  {
    name: 'Multilinie',
    note: {
      title: 'Multilinie',
      content: `
        <b>\\n</b> jest przydatne, ale może być trochę kłopotliwe, jeśli próbujemy sformatować dużo tekstu wielowierszowego. <br> <br>

        Jest jednak inny sposób! Znaki nowej linii są dodawane automatycznie dla ciągów utworzonych przy użyciu trzech cudzysłowów. <br> <br>
        
        <b>Na przykład</b>:

        <div class="code">
        print("""to <br> <br>
        jest <br> <br>
        multiliniowy
        tekst""")
        </div>
        `,
      additionalNote: {
        content: `Ułatwia to formatowanie długich, wielowierszowych tekstów bez konieczności jawnego wstawiania <b>\\n</b> dla łamania wierszy.`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
      Ile wierszy wypisze następujący kod? <br> <br>

      print("Heja") <br>
      print("""To <br>
      jest <br>
      czaderskie <br>
      """)
      `,
      answers: [
        { text: `5`, isCorrect: false },
        { text: `2`, isCorrect: false },
        { text: `3`, isCorrect: false },
        { text: `4`, isCorrect: true }
      ]
    }
  },
  {
    name: 'Koniunkcja',
    note: {
      title: 'Koniunkcja',
      content: `
        W Pythonie matematyka działa zarówno ze słowami, jak i liczbami.

        Możemy więc nie tylko dodawać liczby całkowite i zmiennoprzecinkowe,
        ale także łańcuchy znaków, używając czegoś, co nazywa się <b>koniunkcją</b>.
        Można to zrobić na dowolnych dwóch łańcuchach. W taki sposób:

        <div class="code">print("Spam" + 'eggs')</div>

        Wynikiem powyższego kodu będzie: "Spameggs". <br> <br>

        I działa to nawet z liczbami! Stringi zawierające liczby są nadal dodawane jak stringi, a nie liczby całkowite.

        <div class="code">print("2" + "2")</div>

        Zwróci: "22"
        `,
      additionalNote: {
        content: `Ale nie próbuj dodawać stringa do liczby! Mimo że mogą wyglądać podobnie, są to dwa rózne typy danuch, więc zrobienie tego wywoła błąd.`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Który kod wypisze 77?
      `,
      answers: [
        { text: `print(7 + 7)`, isCorrect: false },
        { text: `print(7 * 2)`, isCorrect: false },
        { text: `print("7" + "7")`, isCorrect: true },
        { text: `"77"`, isCorrect: false }
      ]
    }
  }
]
