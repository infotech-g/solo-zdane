import { EAdditionalNoteType } from 'models/additionalNoteType'
import { ILesson } from 'models/lesson'

export const VariablesLessons: ILesson[] = [
  {
    name: 'Zmienne',
    note: {
      title: 'Zmienne',
      content: `
        Zmienna umożliwia przechowywanie wartości poprzez przypisanie jej do nazwy. <br></br> Nazwa może służyć do późniejszego odwoływania się do wartości w programie.<br>

        Na przykład podczas tworzenia gier użyjesz zmiennej do przechowywania punktów gracza. <br> Jaki jest sens grania, jeśli nie wiesz, kto wygrywa dobrze!
        <br></br>
        Aby przypisać zmienną, użyj jednego <b>znaku równości.</b>

        <div class="code">user = "Jacek"</div>
        `,
      additionalNote: {
        content: `W powyższym przykładzie przypisaliśmy ciąg znaków „Jacek” do zmiennej o nazwie user.`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
        Który kod stworzy zmienną o nazwie "wiek" i przypisze do niej liczbę 42?
      `,
      answers: [
        { text: `wiek = 42`, isCorrect: true },
        { text: '"wiek" = 42', isCorrect: false },
        { text: `wiek = "42"`, isCorrect: false },
        { text: `wiek = 4 + 2`, isCorrect: false }
      ]
    }
  },
  {
    name: 'Nazwy zmiennych',
    note: {
      title: 'Nazwy zmiennych',
      content: `
        Nazywanie zmiennych jest dość elastyczne. <br></br> W nazwach zmiennych można używać <b>liter</b>, <b>cyfr</b> i <b>podkreśleń</b>. <br></br>
        Ale <b>nie możesz</b> używać specjalnych symboli ani zaczynać nazwy od liczby.
        `,
      additionalNote: {
        content: `Nie zapominaj, że Python jest językiem rozróżniającym wielkość liter. Co oznacza, że <b>Nazwisko</b> i <b>nazwisko</b> to dwie różne nazwy zmiennych.`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Która z poniższych jest poprawną nazwą zmiennej w Pythonie?
      `,
      answers: [
        { text: `3ZmIeNnA3`, isCorrect: false },
        { text: 'zmienna%zmienna', isCorrect: false },
        { text: `123"`, isCorrect: false },
        { text: `A_NAZWA_ZMIENNEJ`, isCorrect: true }
      ]
    }
  },
  {
    name: 'Praca ze zmiennymi',
    note: {
      title: 'Praca ze zmiennymi',
      content: `
        Więc masz swoją zmienną i nazwałeś ją. <br></br>Możesz teraz używać jej do wykonywania odpowiednich operacji,
        tak jak robisz to z liczbami i ciągami. 
        <br></br>
        W taki sposób:

        <div class="code">
        x = 7
        print(x + 3)
        </div>

        Wynikiem powyższego programu jest liczba 10.
        `,
      additionalNote: {
        content: `Zmienna przechowuje swoją wartość w całym programie.`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Jaki jest wynik tego kodu?
      <br></br>
        spam = "Jajko"<br>
        print("Wielkie" + spam)
      `,
      answers: [
        { text: `Wielkie Jajko`, isCorrect: false },
        { text: 'WielkieJajko', isCorrect: true },
        { text: `error"`, isCorrect: false },
        { text: `"Wielkiejajko"`, isCorrect: false }
      ]
    }
  },
  {
    name: 'Pobieranie danych wejściowych użytkownika',
    note: {
      title: 'Pobieranie danych wejściowych użytkownika',
      content: `
        Jakiej funkcji użyjemy, aby uzyskać dane wejściowe od użytkownika w Pythonie? <br></br> Proszę o werble! Zgadza się, funkcja <b>input</b>!
        <br></br>
        Python lubi zachować prostotę i intuicyjność.

        Tak więc gra może poprosić o imię i wiek użytkownika jako <b>dane wejściowe</b>, a następnie użyć ich w grze.
        <br></br>
        Funkcja <b>input</b> prosi użytkownika o wprowadzenie danych i zwraca wprowadzony ciąg znaków.

        <div class="code">dane_wejsciowe = input()</div>
        `,
      additionalNote: {
        content: `Nawet jeśli użytkownik wprowadzi liczbę jako dane wejściowe, jest ona przetwarzana jako <b>string</b>`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Jaki jest wynik tego kodu jeżeli użytkownik wprowadzi wartość 3? <br></br>

        x = input()<br>
        print(x + "7")
      `,
      answers: [
        { text: `10`, isCorrect: false },
        { text: 'error', isCorrect: false },
        { text: `37`, isCorrect: true },
        { text: `7`, isCorrect: false }
      ]
    }
  },
  {
    name: 'Operatory miejscowe',
    note: {
      title: 'Operatory miejscowe',
      content: `
        OK, idąc dalej. Porozmawiajmy o <b>operatorach miejscowych</b>. <br></br>

        <b>Operatory miejscowe</b> umożliwiają bardziej zwięzłe pisanie kodu, takiego jak
        „x = x + 3”, jako „x += 3” <br></br>
        A kto nie lubi oszczędzać czasu! 

        Na przykład poniższy kod:
        <div class="code">
        x = 2
        x += 1
        print(x)
        </div>

        Zwróci wartość 3! Kozackie nie?
        `,
      additionalNote: {
        content: `Możemy zrobić to samo z innymi operatorami, takimi jak -, *, / i %.`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
        Jaki jest wynik tego kodu?
      <br></br>
        x = 4<br>
        x *= 3<br>
        print(x)
      `,
      answers: [
        { text: `43`, isCorrect: false },
        { text: '7', isCorrect: false },
        { text: `error`, isCorrect: false },
        { text: `12`, isCorrect: true }
      ]
    }
  }
]
