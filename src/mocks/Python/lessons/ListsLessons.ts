import { EAdditionalNoteType } from 'models/additionalNoteType'
import { ILesson } from 'models/lesson'

export const ListLessons: ILesson[] = [
  {
    name: 'Listy',
    note: {
      title: 'Listy',
      content: `
          Świetna robota, która dotarła tak daleko! W mgnieniu oka zostaniesz mistrzem Pythona. <br></br>

          Zacznijmy ten moduł od omówienia <b>list</b> <br></br>

            W najprostszym przypadku <b>Listy</b> służą do przechowywania przedmiotów. <br></br>
            Listę możemy stworzyć używając nawiasów kwadratowych z przecinkami oddzielającymi elementy.<br></br>
    
                <div class="code"> words = ["Hello", "world", "!"] </div> <br></br>
                W tym przykładzie lista słów zawiera trzy elementy: hello, world i ! <br></br>
                Jeśli chcesz uzyskać dostęp do określonej pozycji na liście, możesz to zrobić, używając jej indeksu w nawiasach kwadratowych. W naszym przykładzie wyglądałoby to tak:

                <div class="code"> words = ["Hello", "world", "!"]
                <br>print("words<b>[0]</b>)
                <br>print("words<b>[1]</b>)
                <br>print("words<b>[2]</b>)
                </div>
                `,
      additionalNote: {
        content: `Indeks pierwszego elementu listy to <b>0</b>, a nie <b>1</b>, jak można się spodziewać..`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
               Jaki jest wynik tego kodu?
               <br></br>
               nums = [5,4,3,2,1]<br>
               print(nums[1])
                `,
      answers: [
        { text: '5', isCorrect: false },
        { text: '4', isCorrect: true },
        { text: '3', isCorrect: false },
        { text: '2', isCorrect: false }
      ]
    }
  },
  {
    name: 'Ciągi jako listy',
    note: {
      title: 'Ciągi jako listy',
      content: `
      <b>Ciągi</b> mogą być indeksowane jak listy! <br></br>


      Indeksowanie <b>ciągu</b> przypomina tworzenie listy zawierającej każdy znak w ciągu. <br></br>
      <b>Dla przykładu:</b>
                <div class="code"> str = "Hello world!" <br> print(str[6])
                </div>
                Spacja (" ") jest również symbolem i ma indeks.
    
    
    
                `,
      additionalNote: {
        content: `Próba uzyskania dostępu do nieistniejącego indeksu spowoduje błąd.`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
               Jaki jest wynik tego kodu?
               <br></br>
               x = "Python" <br>
               print(x[1] + x[4])
                `,
      answers: [
        { text: 'yo', isCorrect: true },
        { text: 'oy', isCorrect: false },
        { text: 'Pn', isCorrect: false },
        { text: 'th', isCorrect: false }
      ]
    }
  },
  {
    name: 'Operacje na listach',
    note: {
      title: 'Operacje na listach',
      content: `
      Aby sprawdzić, czy pozycja znajduje się na określonej liście, możemy użyć operatora <b>in</b> . <br></br>
      Zwraca <b>True</b>, jeśli element występuje na liście raz lub więcej razy, a <b>False</b>, jeśli nie.
                <div class="code"> words = ["spam", "egg", "spam", "sausage"]<br>
                print("spam" <b>in</b> words)<br>
                print("egg" <b>in</b> words)<br>
                print("tomato" <b>in</b> words)<br> 
                </div>
     
    
    
    
                `,
      additionalNote: {
        content: `Operator <b>in</b> służy również do określenia, czy ciąg jest podciągiem innego ciągu.`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
      Jaki jest wynik tego kodu? <br></br>
      nums = [10, 9, 8, 7, 6, 5] <br>

      nums[0] = nums[1] - 5 <br>
      
      if 4 in nums: <br>
      
        <p>&emsp;print(nums[3])</p> 
      
      else: <br>
      
      <p>&emsp;print(nums[4])</p>
      
                `,
      answers: [
        { text: '4', isCorrect: false },
        { text: '3', isCorrect: false },
        { text: '6', isCorrect: false },
        { text: '7', isCorrect: true }
      ]
    }
  },
  {
    name: 'Pętle for',
    note: {
      title: 'Pętle for',
      content: `
      Zawsze jest więcej do powiedzenia, ale to było całkiem dobre wprowadzenie do list. Przejdźmy dalej i wprowadźmy nową pętlę. <br></br>

      Pętla <b>for</b> służy do iteracji po określonej sekwencji, takiej jak listy lub ciągi. <br></br>

      Poniższy kod wyświetla każdą pozycję na liście i dodaje na końcu wykrzyknik:
      
      Zwraca  <div className = "code"> words = ["hello", "world", "spam", "eggs"] <br>
      for word in words: <br>
        <p>&emsp; print(word + "!")</p>
                </div>
                `,
      additionalNote: {
        content: `W powyższym kodzie zmienna word reperezentuje odpowiedni element listy w każdej iteracji. <br></br> 
        Podczas pierwszej iteracji word jest równy "hello", podczas drugiej "world" i tak dalej.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
      Co wypisze ten kod? <br></br>

      letters = ['a', 'b', 'c'] <br>
      for l in letters:
      <p>&emsp;print(l)</p>
        `,
      answers: [
        { text: '123', isCorrect: false },
        { text: 'bca', isCorrect: false },
        { text: 'cab', isCorrect: false },
        { text: 'abc', isCorrect: true }
      ]
    }
  },
  {
    name: 'Metoda range',
    note: {
      title: 'Metoda range',
      content: `
      Python sprawia, że tworzenie sekwencji liczbowych za pomocą funkcji <b>range()</b> jest bardzo łatwe. <br><br>
      Domyślnie zaczyna się od 0, zwiększa się o 1 i kończy przed określoną liczbą. <br></br>
      Poniższy kod generuje listę zawierającą wszystkie liczby całkowite, do 10.


      
    <div class="code">
        numbers = list(<b>range</b>(10)) <br> print(numbers)
    </div>
      
            `,
      additionalNote: {
        content: `Aby wyprowadzić zakres jako listę, musimy jawnie przekonwertować go na listę za pomocą funkcji list().
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: ` 
     Jaki jest wynik tego kodu?

      nums=list(range(5)) <br>
      print(nums[4])
            `,
      answers: [
        { text: '3', isCorrect: false },
        { text: '4', isCorrect: true },
        { text: '2', isCorrect: false },
        { text: '1', isCorrect: false }
      ]
    }
  },
  {
    name: 'Dzielenie listy',
    note: {
      title: 'Dzielenie listy',
      content: `
      Szukasz bardziej zaawansowanego sposobu pobierania wartości z listy? Nie szukaj dalej niż <b>dzielenie list</b>! <br><br>
      Najbardziej podstawowy podział listy polega na indeksowaniu listy za pomocą dwóch liczb całkowitych oddzielonych dwukropkami. Spowoduje to zwrócenie nowej listy zawierającej wszystkie wartości ze starej listy między indeksami. <br></br>
      <b>Dla przykładu:</b>.


      
    <div class="code">
    squares = [0, 1, 4, 9, 16, 25, 36, 49, 64, 81]<br>
    print(squares[2:6])<br>
    print(squares[3:8])<br>
    print(squares[0:1])<br>
    </div>
      
            `,
      additionalNote: {
        content: `Podobnie jak argumenty do range, pierwszy indeks podany w wycinku jest uwzględniany w wyniku, ale drugi nie.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: ` 
     Jaki jest wynik tego kodu?
    <br></br>
      nums=list(range(5)) <br>
      print(nums[4])
            `,
      answers: [
        { text: '3', isCorrect: false },
        { text: '4', isCorrect: true },
        { text: '2', isCorrect: false },
        { text: '1', isCorrect: false }
      ]
    }
  }
]
