import { EAdditionalNoteType } from 'models/additionalNoteType'
import { ILesson } from 'models/lesson'

export const FunctionLessons: ILesson[] = [
  {
    name: 'Funkcje',
    note: {
      title: 'Funkcje',
      content: `
      Zacznijmy od omówienia funkcji. Do tej pory poznałeś i używałeś wielu <b>funkcji</b> (grupy powiązanych ze sobą instrukcji, które wykonują określone zadanie). <br></br>
    
      Aby odświeżyć pamięć, oto kilka przykładów, które już widziałeś:
      <div class="code"> 
      <b>print</b>("Hello world!")<br>
      <b>range</b>(2, 20)<br>
      <b>str</b>(12) <br>
      <b>range</b>(10, 20, 3)
       </div>
       Słowa przed nawiasami są <b>nazwami</b> funkcji, a wartości oddzielone przecinkami wewnątrz nawiasów są <b>argumentami</b> funkcji.



    
                `,
      additionalNote: {
        content: `Funkcje pomagają podzielić nasz program na mniejsze, modułowe porcje. Ponieważ nasz program staje się coraz większy i bardziej złożony, funkcje pomagają uczynić go bardziej zorganizowanym i łatwiejszym w zarządzaniu.
        .`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
                Ile argumentów znajduje się w tym wywołaniu funkcji? <br></br> range(0, 100, 5)
                `,
      answers: [
        { text: '0', isCorrect: false },
        { text: '1', isCorrect: false },
        { text: '3', isCorrect: true },
        { text: '2', isCorrect: false }
      ]
    }
  },
  {
    name: 'Funkcje list',
    note: {
      title: 'Funkcje list',
      content: `
      Python ma kilka przydatnych wbudowanych funkcji. Takich jak  <b>len</b>! <br></br>
    
      <b>len</b> pozwala uzyskać liczbę pozycji na liście. W ten sposób:


      <div class="code"> 
      nums = [1,3,5,2,4] <br>
      print(<b>len</b>(nums))
       </div>
       Możesz również użyć wartości <b>len</b> na ciągach, aby zwrócić ich długość (liczbę znaków).
                `,
      additionalNote: {
        content: `W przeciwieństwie do indeksowania elementów, len nie zaczyna się od 0. Powyższa lista zawiera 5 elementów, co oznacza, że len zwróci 5.`,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
              Jaki jest wynik tego kodu? <br></br>
            letters = ["a", "b", "c"]<br>
            letters += ["d"]<br>
            print(len(letters))<br>

                `,
      answers: [
        { text: '0', isCorrect: false },
        { text: '4', isCorrect: true },
        { text: '3', isCorrect: false },
        { text: '2', isCorrect: false }
      ]
    }
  },
  {
    name: 'Funkcje ciągów',
    note: {
      title: 'Funkcje ciągów',
      content: `
      Ciągi mają funkcję <b>format()</b>, która umożliwia osadzanie w nich wartości za pomocą symboli zastępczych. <br></br>
      <b>Przykład:</b>


      <div class="code"> 
      nums = [4, 5, 6]<br>
msg = "Numbers: {0} {1} {2}". format(nums[0], nums[1], nums[2])<br>
print(msg)

       </div>

                `,
      additionalNote: {
        content: `
        Każdy argument funkcji format jest umieszczany w ciągu w odpowiedniej pozycji, która jest określana za pomocą nawiasów klamrowych { }.
        `,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
              Jaki jest wynik tego kodu? <br></br>
              print("{0}{1}{0}".format("abra", "cad"))
                `,
      answers: [
        { text: 'abracadabra', isCorrect: true },
        { text: 'abracadabar', isCorrect: false },
        { text: 'abracaadbar', isCorrect: false },
        { text: 'abracbcbbar', isCorrect: false }
      ]
    }
  },
  {
    name: 'Twoje własne funkcje',
    note: {
      title: 'Twoje własne funkcje',
      content: `
      Ale co, jeśli te predefiniowane funkcje po prostu nie wystarczają. Potrzebujesz większej elastyczności, 
      aby stworzyć program, nad którym pracujesz. Cóż, możesz tworzyć własne funkcje za pomocą instrukcji <b>def</b> <br></br>

      Oto przykład funkcji o nazwie my_func. Nie przyjmuje żadnych argumentów i trzykrotnie drukuje „spam”. Instrukcje w funkcji są wykonywane tylko wtedy, gdy funkcja jest wywoływana.
        <br></br>

      <div class="code"> 
        <b>def</b> my_func(): <br>
        <p>&emsp;print("spam") <br> </p>
        <p>&emsp;print("spam") <br> </p>
        <p>&emsp;print("spam") <br> </p>
        <br> my_func()

       </div>

                `,
      additionalNote: {
        content: `
        Blok kodu w każdej funkcji zaczyna się od dwukropka (:) i jest wcięcia.
        `,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
              Jak wywołać tą funkcje?
              def hello(): <br> print("Hi!")
                `,
      answers: [
        { text: 'def_hello()', isCorrect: false },
        { text: 'hello()', isCorrect: true },
        { text: 'hello(true)', isCorrect: false },
        { text: 'hello', isCorrect: false }
      ]
    }
  },
  {
    name: 'Argumenty',
    note: {
      title: 'Argumenty',
      content: `
      Funkcje mogą przyjmować <b>argumenty</b>, których można użyć do wygenerowania danych wyjściowych funkcji.
      <br></br> <b>Na przykład:</b>

      <div class="code"> 
      def print_with_exclamation(word):<br>
      <p> &emsp;print(word + "!")</p> <br>

      print_with_exclamation("spam") <br>
      print_with_exclamation("eggs")<br>
      print_with_exclamation("python")<br>
       </div>

                `,
      additionalNote: {
        content: `
        Jak widać na przykładzie, argument jest zdefiniowany w nawiasach i jest nazwany word.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
              Jaki jest wynik tego kodu <br></br>

              def print_double(x):<br>
              <p> &emsp; print(2 * x) </p> <br>
              print_double(3)
                `,
      answers: [
        { text: '4', isCorrect: false },
        { text: '2', isCorrect: false },
        { text: 'x', isCorrect: false },
        { text: '6', isCorrect: true }
      ]
    }
  },
  {
    name: 'Zwracanie wartości',
    note: {
      title: 'Zwracanie wartości',
      content: `
      Niektóre funkcje, takie jak int lub str, zwracają wartość zamiast ją wyprowadzać. <br></br>
      Zwrócona wartość może być wykorzystana w dalszej części kodu, na przykład poprzez przypisanie jej do zmiennej.<br></br.
      Aby to zrobić dla zdefiniowanych funkcji, możesz użyć instrukcji return. <br></br>
        <b>Na przykład:</b>



      <div class="code"> 
        def max(x,y): <br>
        <p> &emsp; if x >= y: <br> &emsp; &emsp; return x </p>
        <p> &emsp; else: <br> &emsp; &emsp; return y </p> <br>

        print(max(4,7))
       </div>

                `,
      additionalNote: {
        content: `
        Instrukcja return nie może być używana poza definicją funkcji
        `,
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
              Jaki jest wynik tego kodu? <br></br>

             def func(x):<br>
              <p> &emsp; return x + 1</p> <br>

              print(func(func(1)))
                `,
      answers: [
        { text: '3', isCorrect: true },
        { text: '1', isCorrect: false },
        { text: '2', isCorrect: false },
        { text: '0', isCorrect: false }
      ]
    }
  }
]
