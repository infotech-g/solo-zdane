import { EAdditionalNoteType } from 'models/additionalNoteType'
import { ILesson } from 'models/lesson'

export const ControlFlowLessons: ILesson[] = [
  {
    name: 'Wartości boolowskie',
    note: {
      title: 'Wartości boolowskie',
      content: `
            Robisz postępy! Dobra robota! Jesteś teraz gotowy zeby poznać 
            nowy tym zmiennych w Pythonie.

            Poznałeś już stringi, intigery i floaty. <br></br> Czas dodać do tego wartości boolowskie.

            Wartości boolowskie mogą przyjmować dwie wartoście: <b>Prawda</b> albo <b>Fałsz.</b>

            Mozęmy tworzyć je przy porównywaniu wartości, dla przykładu za pomocą użycia operatora równości ==.

            <div class="code"> my_boolean = True <br> print(my_boolean) <br> <b>True</b> <br> 
            <br>print(2 == 3) <br> <b>False</b> <br></br>
            print("hello" == "hello")<br> <b>True</b> </div>
            `,
      additionalNote: {
        content: `Uważaj by nie pomylić <b>nadania wartości</b>(jeden znak równości) z <b>porównaniem</b>(dwa znaki równości).`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
            Jakie wartości może przyjmować wartość boolowska?
            `,
      answers: [
        { text: 'true lub false', isCorrect: false },
        { text: 'Truth lub Falsity', isCorrect: false },
        { text: 'True lub False', isCorrect: true },
        { text: 'T lub F', isCorrect: false }
      ]
    }
  },
  {
    name: 'Instrukcja warunkowa if',
    note: {
      title: 'Instrukcja warunkowa if',
      content: `
            Ok, więc nasze porównanie ustaliło, czy nasze stwierdzenie jest prawdziwe czy fałszywe, co teraz?

            Jedną z rzeczy, które możesz zrobić, jest użycie instrukcji if do uruchomienia kodu w oparciu o określony warunek.

            W przypadku instrukcji If, jeśli warunek ma wartość True, instrukcje są wykonywane. W przeciwnym razie nie są przeprowadzane.

            Instrukcja if wygląda w ten sposób:

            <div class="code"> <b>if</b> condition: <br>  <p> &emsp; statements </p
        
            </div>
            `,
      additionalNote: {
        content: `Python używa wcięć (pustej spacji na początku wiersza) 
        do rozgraniczenia bloków kodu. W zależności od logiki programu wcięcia mogą być obowiązkowe.
         Jak widać, stwierdzenia w if powinny być wcięte.`,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
            Jaka część instrukcji if powinna być wcięta?
            `,
      answers: [
        { text: 'Wszystko', isCorrect: false },
        { text: 'Kod zawarty w instrukcji warunkowej', isCorrect: true },
        { text: 'Pierwsza linia', isCorrect: false },
        { text: 'Nic', isCorrect: false }
      ]
    }
  },
  {
    name: 'Instrukcja warunkowa else',
    note: {
      title: 'Instrukcja warunkowa else',
      content: `
      Możemy całkowicie zrobić to samo ze stwierdzeniami, które są fałszywe. 
      Potrzebujemy tylko innej instrukcji. W tym miejscu pojawia się instrukcja else! <br></br>
      Instrukcja <b>else</b> może być używana do uruchamiania niektórych instrukcji, gdy warunkiem instrukcji <b>if</b> jest <b>False</b>.
            <div class="code"> x = 4 <br> <b>if</b> x == 5: <br> <p> &emsp; print("Yes")</p> <br> <b>else</b>: <br>  <p> &emsp; print("No")</p>  
            </div>
            `,
      additionalNote: {
        content: `Dwukropek na końcu słowa kluczowego else jest ważny, nie pomijaj go.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: ` 
        Jaki jest wynik tego kodu?

      if 1 + 1 == 2:

      if 2 * 2 == 8:
   
         print("if")
   
      else:
   
         print("else") 
            `,
      answers: [
        { text: 'else', isCorrect: true },
        { text: 'if', isCorrect: false },
        { text: 'Null', isCorrect: false },
        { text: '8', isCorrect: false }
      ]
    }
  },
  {
    name: 'Logika boolowska',
    note: {
      title: 'Logika boolowska',
      content: `
        Czas ulepszyć nasze wartości boolowskie za pomoca kilku operatorów!

        Boolowskie operatory <b>and, or, not</b> pozwalają na spardzanie kilku warunków w jednej instrukcji warunkowej. <br></br>

        Zacznijmy od operatora <b>and</b>. Zwraca on True gdy oba warunki zwracają True:

            <div class="code"> print( 1 == 1 <b>and</b> 2 == 2) <br> <b>True</b>
            <br></br> print(1 == 1 <b>and</b> 2 == 3) <br> <b>False</b>
            </div>
            `,
      additionalNote: {
        content: `Dwukropek na końcu słowa kluczowego else jest ważny, nie pomijaj go.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: ` 
        Jaki jest wynik tego kodu?
<br>
        if (1 == 1) and (2 + 2 > 3): <br>

        <p> &emsp;print("true")</p>
      
      else:
        <br>
        <p> &emsp;print("false")</p>
            `,
      answers: [
        { text: 'false', isCorrect: false },
        { text: 'true', isCorrect: true },
        { text: 'true false', isCorrect: false },
        { text: '4', isCorrect: false }
      ]
    }
  },
  {
    name: 'Pentla while',
    note: {
      title: 'Pentla while',
      content: `
      Możemy użyć pętli <b>while</b> do wielokrotnego powtórzenia bloku kodu. <br></br>

        Załóżmy na przykład, że musimy przetworzyć wiele danych wejściowych użytkownika, aby za każdym razem, gdy użytkownik coś wprowadzi, 
        ten sam blok kodu musi zostać wykonany.
        <br></br>
        Oto pętla <b>while</b> zawierająca zmienną, która liczy od 1 do 5.

            <div class="code"> i = 1 <br> <b>while</b> i <=5: <br> <p> &emsp; print(i)<br> &emsp; i = i + 1 </p>
            <br> print("Finished!)
            </div>
        Podczas każdej iteracji pętli zmienna i jest zwiększana o jeden, aż osiągnie 5.
        Tak więc pętla wykona polecenie print 5 razy.
            `,
      additionalNote: {
        content: `Kod w treści pętli <b>while</b> jest wykonywany wielokrotnie. Nazywa się to <b>iteracją</b>.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: ` 
      Ile cyfr wypisuje ten kod? <br>
        <br>
      i = 3
      <br>

      while i>=0: <br>
      
      <p> &emsp;print(i) </p>
      
      <p> &emsp;i = i - 1 </p><br>
            `,
      answers: [
        { text: '3', isCorrect: false },
        { text: '4', isCorrect: true },
        { text: '2', isCorrect: false },
        { text: '1', isCorrect: false }
      ]
    }
  },
  {
    name: 'Instrukcja break',
    note: {
      title: 'Instrukcja break',
      content: `
      Nieskończone pętle mogą brzmieć fajnie, ale nie zawsze są przydatne. <br><br>
      Aby przedwcześnie zakończyć pętlę <b>while</b>, możemy użyć instrukcji <b>break</b>. <br></br>
      Na przykład możemy przerwać nieskończoną pętlę, jeśli spełniony jest jakiś warunek:


      
    <div class="code">
    <p>i = 0 <br> while True: <br> &emsp;print(i) <br> &emsp;i = i + 1 <br> &emsp;if i >= 5: <br>  &emsp;&emsp;print("Breaking)<br>&emsp;&emsp;break</p>
    </div>
      
            `,
      additionalNote: {
        content: `Użycie instrukcji break poza pętlą powoduje błąd.
        `,
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: ` 
      Ile cyfr wypisuje ten kod? <br>
        <br>
      i = 3
        <br>
      while i>=0:
      <br>
      <p> &emsp;print(i)</p>
      
      <p> &emsp;i = i - 1</p
            `,
      answers: [
        { text: '3', isCorrect: false },
        { text: '4', isCorrect: true },
        { text: '2', isCorrect: false },
        { text: '1', isCorrect: false }
      ]
    }
  }
]
