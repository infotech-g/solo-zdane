import { EAdditionalNoteType } from 'models/additionalNoteType'
import { ILesson } from 'models/lesson'

export const BasicConceptLessons: ILesson[] = [
  {
    name: 'Witaj w Pythonie!',
    note: {
      title: 'Czym jest Python?',
      content: `
        Chcesz zbudować zgrabną stronę internetową? Stworzyć grę wideo? A może zaprogramować sztuczną inteligencję?
        Z Pythonem to wszystko staje się możliwe, a nawet więcej. <br> <br>
        
        Python to <b>język programowania</b> wysokiego poziomu z mnóstwem bibliotek. Jest bardzo elastyczny i dostępny, co 
        czyni go popularnym wśród niektórych największych (i najfajniejszych) organizacji na świecie – pomyśl o Google,
        NASA, Disney…*szepty* nawet CIA. <br> <br>

        Na tym kursie omówimy <b>podstawowe koncepcje</b> Pythona, a także stworzymy rzeczywiste projekty i rozwiążemy różne wyzwania związane z kodowaniem!
        `,
      additionalNote: {
        content:
          'Będziemy uczyć się Pythona w wersji 3, który jest najnowszą główną wersją Pythona.',
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
        Każda lekcja będzie podsumowywana quizem stworzonym by pomóc Ci ćwiczyć i zapamiętać nauczone koncepty. <br> <br>
        
        Gotowy? No to zaczynajmy! <br> <br>

        Która wersja Pythona będzie omawiana w tym kursie?
      `,
      answers: [
        { text: 'Python 3', isCorrect: true },
        { text: 'Python 7', isCorrect: false },
        { text: 'Python 2', isCorrect: false },
        { text: 'Python 5', isCorrect: false }
      ]
    }
  },
  {
    name: 'Witaj świecie',
    note: {
      title: 'Witaj świecie',
      content: `
        Zacznijmy od stworzenia prostego programu, który wyświetla tekst „Witaj świecie!”. <br> <br>

        W Pythonie do wypisywania tekstu używamy funkcji <b>print</b>.

        Aby wygenerować naszą wiadomość, kod wyglądałby tak:

        <div class="code">print('Witaj świecie!')</div>

        Dobra robota! Właśnie napisałeś swój pierwszy program w Pythonie. Łatwe, prawda? W mgnieniu oka załapiesz resztę!
        `,
      additionalNote: {
        content:
          'Po instrukcji <b>print</b> zawsze znajdują się <b>nawiasy</b> zawierające dane wyjściowe, które chcemy wygenerować.',
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Jakiej instrukcji użyjesz by wyświetlić na ekranie napis?
      `,
      answers: [
        { text: 'write', isCorrect: false },
        { text: 'output', isCorrect: false },
        { text: 'print', isCorrect: true },
        { text: 'cout', isCorrect: false }
      ]
    }
  },
  {
    name: 'Proste operacje',
    note: {
      title: 'Proste operacje',
      content: `
        Więc Python umie mówić, ale czy umie liczyć? <br> <br>
        Wykonanie obliczeń w Pythonie jest proste, wystarczy wpisać je do instrukcji
        <b>print</b> (nie zapomnij o nawiasach!):

        <div class="code">
        print(2 + 2) <br> <br>
        print(5 + 4 - 3)
        </div>
        `,
      additionalNote: {
        content:
          'Widzisz spacje wokół znaków plus i minus? Kod działałby bez nich, ale znacznie ułatwiają czytanie.',
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
        Co wyświetli ten kod? <br> <br>

        print(1 + 2 + 3)
      `,
      answers: [
        { text: '6', isCorrect: true },
        { text: '5', isCorrect: false },
        { text: '1 2 3', isCorrect: false },
        { text: '123', isCorrect: false }
      ]
    }
  },
  {
    name: 'Typy danych',
    note: {
      title: 'Typy danych',
      content: `
        Zanim przejdziemy dalej, warto przedstawić główne typy danych, których używamy w Pythonie. Każda wartość w Pythonie ma swój typ. <br> <br>

        Ciąg znaków, taki jak „Witaj świecie”, nazywa się <b>string</b>. <br>
        Liczby całkowite nazywane są <b>int</b>. <br>
        A liczby zmiennoprzecinkowe <b>float</b>. <br> <br>

        Teraz, skoro napotkaliśmy ciągi i liczby całkowite... Czas na liczenie!
        `,
      additionalNote: {
        content:
          'Niektóre przykłady liczb reprezentowanych jako zmiennoprzecinkowe to 0,5 i -7,8237591.',
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Która z tych wartości byłaby przechowywana jako float?
      `,
      answers: [
        { text: '"hey"', isCorrect: false },
        { text: '42', isCorrect: false },
        { text: '2022', isCorrect: false },
        { text: '7.2', isCorrect: true }
      ]
    }
  },
  {
    name: 'Potęgowanie',
    note: {
      title: 'Potęgowanie',
      content: `
        W porządku, więc znasz już podstawowe operacje, dodawanie, odejmowanie, mnożenie i dzielenie. Świetnie Ci idzie! <br> <br>

        Czas podnieść poprzeczkę i wprowadzić potęgowanie – tak to nazywamy, gdy podnosimy jedną liczbę do potęgi drugiej.
        Operacja potęgowania jest wykonywana przy użyciu <b>dwóch gwiazdek</b>. <br><br> 
        
        W ten sposób:

        <div class="code">print( 2**5 )</div>
        `,
      additionalNote: {
        content:
          'Możesz łączyć potęgi razem. Innymi słowy, możesz zwiększyć liczbę do wielu potęg. Na przykład 2**3**2.',
        type: EAdditionalNoteType.Funfact
      }
    },
    quiz: {
      question: `
        Co należy wpisać wewnątrz instrukcji print() by program wypisał 5 do potęgi 3?
      `,
      answers: [
        { text: '3**5', isCorrect: false },
        { text: 'potega(5, 3)', isCorrect: false },
        { text: '5**3', isCorrect: true },
        { text: '5*5', isCorrect: false }
      ]
    }
  },
  {
    name: 'Reszta z dzielenia',
    note: {
      title: 'Reszta z dzielenia',
      content: `
        Ach, te nieznośne pozostałości. Ale nie są tak nieznośne w Pythonie. <br> <br>

        Możemy użyć operatora <b>modulo</b> – który jest wykonywany symbolem procentu (%)
        – aby uzyskać <b>resztę</b> z danego dzielenia. <br> <br>

        <b>Na przykład:</b> 
        <div class="code">
        print(20 % 6) <br> <br>
        print(1.25 % 0.5)
        </div>
        `,
      additionalNote: {
        content:
          'Wszystkie operatory numeryczne mogą być również używane z liczbami zmiennoprzecinkowymi.',
        type: EAdditionalNoteType.Warning
      }
    },
    quiz: {
      question: `
        Jaki jest wynik tego kodu? <br> <br>

        print(7 % 3)
      `,
      answers: [
        { text: '1', isCorrect: true },
        { text: '7', isCorrect: false },
        { text: '0', isCorrect: false },
        { text: 'error', isCorrect: false }
      ]
    }
  }
]
