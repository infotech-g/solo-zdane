import { ISection } from 'models/section'
import { BasicConceptLessons } from './lessons/BasicConceptLessons'
import { ControlFlowLessons } from './lessons/ControlFlowLesson'
import { FunctionLessons } from './lessons/FunctionLessons'
import { ListLessons } from './lessons/ListsLessons'
import { StringLessons } from './lessons/StringsLessons'
import { VariablesLessons } from './lessons/VariablesLessons'

export const Sections: ISection[] = [
  {
    title: 'Podstawowe koncepty/założenia',
    lessons: BasicConceptLessons
  },
  {
    title: 'Ciągi znaków',
    lessons: StringLessons
  },
  {
    title: 'Zmienne',
    lessons: VariablesLessons
  },
  {
    title: 'Przepływ sterowania',
    lessons: ControlFlowLessons
  },
  {
    title: 'Listy',
    lessons: ListLessons
  },
  {
    title: 'Funkcje',
    lessons: FunctionLessons
  }
]
