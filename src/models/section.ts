import { ILesson } from './lesson'

export interface ISection {
  title: string
  lessons: ILesson[]
}
