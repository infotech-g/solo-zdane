import { EAdditionalNoteType } from './additionalNoteType'

export interface IAdditionalNote {
  content: string
  type: EAdditionalNoteType
}
