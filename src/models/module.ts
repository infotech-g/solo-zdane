import { ISection } from './section'

export interface IModule {
  id: number
  title: string
  description: string
  icon: string
  sections: ISection[]
}
