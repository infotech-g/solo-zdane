import { INote } from './note'
import { IQuiz } from './quiz'

export interface ILesson {
  name: string
  note: INote
  quiz: IQuiz
}
