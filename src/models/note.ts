import { IAdditionalNote } from './addtionalNote'

export interface INote {
  title: string
  content: string
  additionalNote: IAdditionalNote
}
