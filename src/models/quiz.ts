import { IAnswer } from './answer'

export interface IQuiz {
  question: string
  answers: IAnswer[]
}
