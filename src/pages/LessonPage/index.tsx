import LessonFooter from 'components/LessonFooter'
import LessonHeader from 'components/LessonHeader'
import Note from 'components/Note'
import Quiz from 'components/Quiz'
import { PATHS } from 'consts'
import { Sections } from 'mocks/Python/sections'
import { useState } from 'react'
import { generatePath, useParams } from 'react-router-dom'
import './styles.scss'

const LessonPage = () => {
  const { sectionID, lessonID } = useParams()
  const [lessonStep, setLessonStep] = useState(0)

  const convertedSectionID = parseInt(sectionID || '')
  const convertedLessonID = parseInt(lessonID || '')
  const lesson = Sections[convertedSectionID].lessons[convertedLessonID]
  const isLastLesson =
    convertedLessonID + 1 === Sections[convertedSectionID].lessons.length
  const nextLessonPath = generatePath(PATHS.lesson, {
    lessonID: convertedLessonID + 1,
    sectionID
  })

  const renderStep = () => {
    switch (lessonStep) {
      case 0:
        return <Note {...lesson.note} />
      case 1:
        return <Quiz {...lesson.quiz} />
    }
  }

  return (
    <div className='lesson-page'>
      <LessonHeader />
      {renderStep()}
      <LessonFooter
        lessonStep={lessonStep}
        isLastLesson={isLastLesson}
        nextLessonPath={nextLessonPath}
        setLessonStep={setLessonStep}
      />
    </div>
  )
}

export default LessonPage
