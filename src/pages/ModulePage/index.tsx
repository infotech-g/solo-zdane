import ModuleHeader from 'components/ModuleHeader'
import SectionsList from 'components/SectionsList'
import { Sections } from 'mocks/Python/sections'
import './styles.scss'

const ModulePage = () => {
  return (
    <div className='module-page'>
      <ModuleHeader title='Python' />
      <SectionsList sections={Sections} />
    </div>
  )
}

export default ModulePage
