export const PATHS = {
  modules: '/play-with-python',
  lesson: '/play-with-python/sections/:sectionID/lessons/:lessonID'
}
