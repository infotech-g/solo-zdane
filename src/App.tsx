import { PATHS } from 'consts'
import LessonPage from 'pages/LessonPage'
import ModulePage from 'pages/ModulePage'
import QuizProvider from 'providers/quiz'
import { Route, Routes } from 'react-router-dom'
import './App.css'

const App = () => (
  <Routes>
    <Route path={PATHS.modules} element={<ModulePage />} />
    <Route
      path={PATHS.lesson}
      element={
        <QuizProvider>
          <LessonPage />
        </QuizProvider>
      }
    />
  </Routes>
)

export default App
