import MUIButton from '@mui/material/Button'
import { ReactNode } from 'react'
import './styles.scss'

export interface ButtonProps {
  /** Other prompts to pass */
  children: ReactNode
  /** CSS class */
  className?: string
  /** Boolean determining if button is enabled */
  disabled?: boolean
  /** Type of button - text, outlined, contained */
  type?: 'text' | 'outlined' | 'contained'
  /** Function that invokes on click event */
  onClick: () => void
}

/**
 * Default button using MUI library
 */
const Button = ({
  children,
  disabled,
  type,
  className,
  onClick
}: ButtonProps) => {
  return (
    <MUIButton
      id='button'
      className={className}
      disabled={disabled}
      variant={type}
      onClick={onClick}>
      {children}
    </MUIButton>
  )
}

export default Button
