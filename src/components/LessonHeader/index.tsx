import { faChevronLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { PATHS } from 'consts'
import { Link } from 'react-router-dom'
import './styles.scss'

/**
 * An header of the lesson containing arrow letting to go back
 */
const LessonHeader = () => {
  return (
    <div className='lesson-header'>
      <Link className='lesson-header__back-btn' to={PATHS.modules}>
        <FontAwesomeIcon className='back-btn__icon' icon={faChevronLeft} />
        <p className='back-btn__text'>Sekcje</p>
      </Link>
    </div>
  )
}

export default LessonHeader
