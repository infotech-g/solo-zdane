import SectionItem from 'components/SectionItem'
import { ISection } from 'models/section'
import './styles.scss'

export interface SectionTilesListProps {
  /** List of section's objects */
  sections: ISection[]
}

/**
 * List of sections
 */
const SectionsList = ({ sections }: SectionTilesListProps) => {
  return (
    <div className='sections-list'>
      {sections.map((section, index) => (
        <SectionItem key={index} sectionID={index} section={section} />
      ))}
    </div>
  )
}

export default SectionsList
