import parse from 'html-react-parser'
import './styles.scss'
import AnswersList from 'components/AnswersList'
import { IAnswer } from 'models/answer'

export interface QuizProps {
  /** Question text */
  question: string
  /** List of answers with information's about correctness */
  answers: IAnswer[]
}

/**
 * Quiz modal for the lesson detail view
 */
const Quiz = ({ question, answers }: QuizProps) => {
  return (
    <div className='quiz'>
      <p className='quiz__question'>{parse(question)}</p>
      <AnswersList answers={answers} />
    </div>
  )
}

export default Quiz
