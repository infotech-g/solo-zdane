import classNames from 'classnames'
import './styles.scss'

export interface SectionTileProps {
  /** Title of the section */
  title: string
  /** Boolean that determines if section was opened */
  isOpened: boolean
  /** Function that invokes on click event */
  onClick: () => void
}

/**
 * Section button that can be clicked
 */
const SectionTile = ({ title, isOpened, onClick }: SectionTileProps) => {
  return (
    <div
      className={classNames('section-tile', {
        'section-tile--opened': isOpened
      })}
      onClick={onClick}>
      <p className='section-tile__title'>{title}</p>
    </div>
  )
}

export default SectionTile
