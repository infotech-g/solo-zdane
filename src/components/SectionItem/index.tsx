import LessonsList from 'components/LessonsList'
import SectionTile from 'components/SectionTile'
import { ISection } from 'models/section'
import { useState } from 'react'
import './styles.scss'

export interface SectionItemProps {
  /** Section's identificator */
  sectionID: number
  /** Object containing information's about that section */
  section: ISection
}

/**
 * One section container that can be dropped down with the list of lessons
 */
const SectionItem = ({ sectionID, section }: SectionItemProps) => {
  const [showLessons, setShowLessons] = useState(false)

  const onSectionTileClick = () => {
    setShowLessons((prev) => !prev)
  }

  return (
    <div className='section-item'>
      <SectionTile
        title={section.title}
        isOpened={showLessons}
        onClick={onSectionTileClick}
      />
      {showLessons && (
        <LessonsList sectionID={sectionID} lessons={section.lessons} />
      )}
    </div>
  )
}

export default SectionItem
