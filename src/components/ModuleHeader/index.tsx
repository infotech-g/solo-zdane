import './styles.scss'

export interface ModuleHeaderProps {
  /** Title of the application */
  title: string
}

/**
 * Header of the main view
 */
const ModuleHeader = ({ title }: ModuleHeaderProps) => {
  return (
    <div className='module-header'>
      <img
        className='module-header__img'
        src={require('assets/python-logo.png')}
      />
      <div className='module-header__text-container'>
        <h1>{title}</h1>
        <p>
          Python to popularny, łatwy do nauczenia i bardzo wydajny język
          programowania, który jest używany w tworzeniu oprogramowania i
          tworzenia stron internetowych, nauce o danych, uczeniu maszynowym i
          wielu innych dziedzinach. Na tym kursie omówimy podstawowe koncepcje
          Pythona, a także stworzymy rzeczywiste projekty i rozwiążemy różne
          wyzwania związane z kodowaniem. Python dla początkujących nie wymaga
          wcześniejszego doświadczenia w programowaniu, więc bierzmy się do
          roboty!
        </p>
      </div>
    </div>
  )
}

export default ModuleHeader
