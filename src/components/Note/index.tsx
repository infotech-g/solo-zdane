import AdditionalNote from 'components/AdditionalNote'
import parse from 'html-react-parser'
import { IAdditionalNote } from 'models/addtionalNote'
import './styles.scss'

export interface NoteProps {
  /** Title of note */
  title: string
  /** Text with formattings */
  content: string
  /** Additional note object */
  additionalNote: IAdditionalNote
}

/**
 * Note modal for the lesson detail view
 */
const Note = ({ title, content, additionalNote }: NoteProps) => {
  return (
    <div className='note'>
      <div className='note__title'>{parse(title)}</div>
      <div className='note__content'>{parse(content)}</div>
      <AdditionalNote {...additionalNote} />
    </div>
  )
}

export default Note
