import classNames from 'classnames'
import { IAnswer } from 'models/answer'
import { useQuiz } from 'providers/quiz/context'
import './styles.scss'

interface AnswerTileProps {
  /** Answer with information about correctness */
  answer: IAnswer
}

/**
 * One answer button that may be clicked
 */
const AnswerTile = ({ answer }: AnswerTileProps) => {
  const { isQuizResolved, clickedAnswerText, resolveQuiz } = useQuiz()

  const { text, isCorrect } = answer
  const isClicked = clickedAnswerText === answer.text

  const onClick = () => {
    if (!isQuizResolved) {
      resolveQuiz(isCorrect, text)
    }
  }

  return (
    <div
      className={classNames('answer-tile', {
        'answer-tile--correct': isQuizResolved && isClicked && isCorrect,
        'answer-tile--incorrect': isQuizResolved && isClicked && !isCorrect
      })}
      onClick={onClick}>
      {text}
    </div>
  )
}

export default AnswerTile
