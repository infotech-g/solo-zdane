import Button from 'components/Button'
import { PATHS } from 'consts'
import { useQuiz } from 'providers/quiz/context'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import './styles.scss'

export interface LessonFooterProps {
  /** Lesson stage identifer */
  lessonStep: number
  /** Variable that says if lesson is last in section */
  isLastLesson: boolean
  /** Next lesson route path */
  nextLessonPath: string
  /** Function that allows to manipulate with lesson stage identifer value */
  setLessonStep: React.Dispatch<React.SetStateAction<number>>
}

/**
 * Footer of lesson detail view containing buttons
 */
const LessonFooter = ({
  lessonStep,
  isLastLesson,
  nextLessonPath,
  setLessonStep
}: LessonFooterProps) => {
  const navigate = useNavigate()
  const { isAnswerCorrect, isQuizResolved, resetQuiz } = useQuiz()

  const prevLessonStep = () => setLessonStep((prev) => prev - 1)
  const nextLessonStep = () => setLessonStep((prev) => prev + 1)
  const resetLessonSteps = () => setLessonStep(0)

  const getEndingButtonFunc = () => {
    if (isQuizResolved && !isAnswerCorrect) {
      return resetQuiz
    } else if (isLastLesson) {
      return () => navigate(PATHS.modules)
    } else {
      return () => {
        navigate(nextLessonPath)
        resetLessonSteps()
        resetQuiz()
      }
    }
  }

  return (
    <div className='lesson-footer'>
      <div className='lesson-footer__buttons-wrapper'>
        {lessonStep === 0 ? (
          <Button
            className='buttons-wrapper__btn buttons-wrapper__btn--right'
            type='contained'
            onClick={nextLessonStep}>
            Kontynuuj
          </Button>
        ) : (
          <>
            <Button
              className='buttons-wrapper__btn buttons-wrapper__btn'
              type='outlined'
              onClick={prevLessonStep}>
              Cofnij
            </Button>
            <Button
              className='buttons-wrapper__btn buttons-wrapper__btn'
              type='contained'
              disabled={!isQuizResolved}
              onClick={getEndingButtonFunc()}>
              {isQuizResolved && !isAnswerCorrect
                ? 'Ponów'
                : isLastLesson
                ? 'Zakończ'
                : 'Kontynuuj'}
            </Button>
          </>
        )}
      </div>
    </div>
  )
}

export default LessonFooter
