import './styles.scss'

export interface LessonTileProps {
  /** Lesson indentificator */
  index: number
  /** Name of the lesson */
  name: string
  /** Function redirecting to lesson's detail view */
  onClick: () => void
}

/**
 * Single lesson button navigating to lesson's detail view
 */
const LessonTile = ({ index, name, onClick }: LessonTileProps) => {
  return (
    <div className='lesson-tile' onClick={onClick}>{`${index}. ${name}`}</div>
  )
}

export default LessonTile
