import { faCircleInfo } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import classNames from 'classnames'
import { EAdditionalNoteType } from 'models/additionalNoteType'
import './styles.scss'
import parse from 'html-react-parser'

export interface AdditionalNoteProps {
  /** Additional note object with text and type */
  content: string
  /** Type of that note */
  type: EAdditionalNoteType
}

/**
 * Additional note at the end of every lesson with hints
 */
const AdditionalNote = ({ content, type }: AdditionalNoteProps) => {
  return (
    <div
      className={classNames('additional-note', {
        'additional-note--warning': type === EAdditionalNoteType.Warning,
        'additional-note--funfact': type === EAdditionalNoteType.Funfact
      })}>
      <FontAwesomeIcon className='addtional-note__icon' icon={faCircleInfo} />
      <p className='addition-note__content'>{parse(content)}</p>
    </div>
  )
}

export default AdditionalNote
