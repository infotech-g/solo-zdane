import LessonTile from 'components/LessonTile'
import { PATHS } from 'consts'
import { ILesson } from 'models/lesson'
import { generatePath, useNavigate } from 'react-router-dom'
import './styles.scss'

export interface LessonsListProps {
  /** Section identificator */
  sectionID: number
  /** List of lessons */
  lessons: ILesson[]
}

/**
 * List of lessons avaiable for particular section
 */
const LessonsList = ({ sectionID, lessons }: LessonsListProps) => {
  const navigate = useNavigate()

  const handleClick = (lessonID: number) => {
    const path = generatePath(PATHS.lesson, { lessonID, sectionID })

    navigate(path)
  }

  return (
    <div className='lessons-list'>
      {lessons.map((lesson, index) => (
        <LessonTile
          key={index}
          index={index + 1}
          onClick={() => handleClick(index)}
          {...lesson}
        />
      ))}
    </div>
  )
}

export default LessonsList
