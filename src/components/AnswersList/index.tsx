import AnswerTile from 'components/AnswerTile'
import { IAnswer } from 'models/answer'
import './styles.scss'

interface AnswersListProps {
  /** List of answers */
  answers: IAnswer[]
}

/**
 * Generates list of answers
 */
const AnswersList = ({ answers }: AnswersListProps) => {
  return (
    <div className='answers-list'>
      {answers.map((answer, index) => (
        <AnswerTile key={index} answer={answer} />
      ))}
    </div>
  )
}

export default AnswersList
