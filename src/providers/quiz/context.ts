import { createContext, useContext } from 'react'
import { QuizContextProps } from './interface'

export const QuizContext = createContext<QuizContextProps>(
  {} as QuizContextProps
)

export const useQuiz = () => useContext(QuizContext)
