import { ReactNode } from 'react'

export interface QuizContextProps {
  isQuizResolved: boolean
  isAnswerCorrect: boolean | undefined
  clickedAnswerText: string | undefined
  resolveQuiz: (isAnswerCorrect: boolean, clickedAnswerText: string) => void
  resetQuiz: () => void
}

export interface QuizProviderProps {
  children: ReactNode
}
