import { useState } from 'react'
import { QuizContext } from './context'
import { QuizProviderProps } from './interface'

const QuizProvider = ({ children }: QuizProviderProps) => {
  const [isQuizResolved, setIsQuizResolved] = useState(false)
  const [isAnswerCorrect, setIsAnswerCorrect] = useState<boolean | undefined>()
  const [clickedAnswerText, setClickedAnswerText] = useState<
    string | undefined
  >()

  const resolveQuiz = (isAnswerCorrect: boolean, clickedAnswerText: string) => {
    setIsAnswerCorrect(isAnswerCorrect)
    setIsQuizResolved(true)
    setClickedAnswerText(clickedAnswerText)
  }

  const resetQuiz = () => {
    setIsQuizResolved(false)
    setIsAnswerCorrect(undefined)
    setClickedAnswerText(undefined)
  }

  return (
    <QuizContext.Provider
      value={{
        isQuizResolved,
        isAnswerCorrect,
        clickedAnswerText,
        resolveQuiz,
        resetQuiz
      }}>
      {children}
    </QuizContext.Provider>
  )
}

export default QuizProvider
